# !/usr/bin/env python
# -*- coding: utf:8 -*-
from Groups.DocGroup import DocGroup
from Account.AccountPayment import AccountPayment
from time import sleep
from Account.AccountLimited import AccountPaymentLimited

grp = DocGroup()

n1 = AccountPayment()
n1.number = 'n1'
n1.address = "Naberezhnaya kanala Griboyedova," \
                " 160," \
                " Sankt-Peterburg," \
                " 190121"
n1.add_post('position1', 1, 100)
n1.add_post('position2', 53, 20)

grp.add(n1)

sleep(0.5)

n2 = AccountPaymentLimited(hours=2)
n2.number = 'n2'
n2.address = "addres 2"
n2.send_to()
n2.receive()
n2.add_post('position1', 2, 300)
n2.add_post('position2', 3, amount=200)
grp.add(n2)

sleep(0.5)

n2 = AccountPaymentLimited(hours=2)
n2.number = 'n3'
n2.address = "addres 232131231232"
n2.send_to()
n2.add_post('position1', 2, 12)
n2.add_post('position2', 3, amount=1111)
grp.add(n2)

# print(grp.as_xml_text)

with open('XML\data.xml', 'wt', encoding='utf-8') as trg:
    print(grp.as_xml_text, file=trg)
