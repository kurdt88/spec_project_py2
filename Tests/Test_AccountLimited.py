from Account.AccountLimited import AccountPaymentLimited

t_acc = AccountPaymentLimited(hours=0)
t_acc.number = 'A-011'
t_acc.address = "Naberezhnaya kanala Griboyedova," \
                " 160," \
                " Sankt-Peterburg," \
                " 190121"
t_acc.add_post('mouse', 100, amount=1200.03)
t_acc.add_post('keyboard', 100, 2560)
t_acc.add_post('monitor', 2000, 34000)
t_acc.send_to()
t_acc.output()
