# /usr/bin/env python3
# -*- coding: utf-8 -*-
from Account.AccountPayment import AccountPayment
from time import sleep
from decimal import Decimal

x = AccountPayment()
x.number = 'A-012'
x.address = "Admiralty Embankment, Saint Petersburg"
x.add_post('mouse', 100, amount=Decimal('1200'))
x.add_post('keyboard', 100, 2560)
x.add_post('monitor', 2000, 34000)
x.send_to()
sleep(1.5)
x.send_to()
# sleep(1.5)
# x.receive()
x.output()
# print(x.is_good)
