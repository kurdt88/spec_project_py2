# !/usr/bin/env python
# -*- coding: utf:8 -*-

from xml.dom.minidom import parse
from Groups import DocGroup

with open('C:\\Users\\kurdt88\\PycharmProjects\\spec_project\\XML\\data.xml', mode='rb') as src:
    data = parse(src)

root = data.documentElement
grp = DocGroup.from_xml(root)

for doc in grp:
    doc.output()
