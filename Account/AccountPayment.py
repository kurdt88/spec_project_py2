# /usr/bin/env python3
# -*- coding: utf-8 -*-
import sys
from decimal import Decimal
from datetime import datetime
from contextlib import suppress

from Groups.Positions.AccountPosition import AccPos
from Groups.Positions import AccPosAmount
from Helpers.exceptions import InsufficientData


class AccountPayment(object):

    def __init__(self):
        self.__pos_list = []
        self.__creation_date = datetime.now()

    @property
    def is_good(self):
        return self.__is_good

    @is_good.getter
    def is_good(self):
        result = Decimal('0.00')
        for pos in self.__pos_list:
            result += pos.amount
            if not pos.is_good:
                return False
        else:
            return result == self.amount_result

    @property
    def recv_time(self):
        return self.__recv_time

    @property
    def is_received(self):
        try:
            self.__recv_time
            return True
        except AttributeError:
            return False

    @property
    def in_way(self):
        if not self.is_out:
            raise InsufficientData('out time')
        if self.is_received:
            return self.recv_time - self.out_time
        else:
            return datetime.now() - self.out_time

    def receive(self):
        self.__recv_time = datetime.now()

    @property
    def out_time(self):
        return self.__out_time

    @property
    def is_out(self):
        try:
            self.__out_time
            return True
        except AttributeError:
            return False

    def send_to(self):
        try:
            self.__address
        except AttributeError as excp:
            print('In payment', self.number, 'from', self.creation_date,
                  'address is not set', file=sys.stderr)
            raise InsufficientData('address') from excp
        self.__out_time = datetime.now()

    @property
    def creation_date(self):
        return self.__creation_date

    @creation_date.setter
    def creation_date(self, creation_date):
        self.__creation_date = creation_date

    @property
    def number(self):
        return self.__number

    @number.setter
    def number(self, new_number):
        try:
            print('Changed number ', self.number, ' to ', new_number, file=sys.stderr)
        except AttributeError:
            print('Set number ', new_number, file=sys.stderr)
        self.__number = new_number

    @property
    def address(self):
        return self.__address

    @address.setter
    def address(self, new_address):
        self.__address = new_address

    @property
    def amount_result(self):
        result = Decimal('0.00')
        for pos in self.__pos_list:
            result += pos.amount
        return result

    def add_post(self, title, count, price=None, *, amount=None):
        if amount is None:
            pos = AccPos(title, count, price)
        else:
            pos = AccPosAmount(title, count, amount)
        self.__pos_list.append(pos)

    # def output(self, *, file=sys.stdout):
    #     print('*' * 40)
    #     print('Payment №', self.number, 'from', self.creation_date)
    #     print('-' * 40)
    #     try:
    #         print('Address: ', self.address)
    #         print('-' * 40)
    #     except AttributeError:
    #         pass
    #     with suppress(AttributeError):
    #         print('Is get at', self.recv_time)
    #         print('Is out at', self.out_time)
    #     with suppress(InsufficientData):
    #         print('In way time:', self.in_way)
    #     print('-' * 40)
    #     for num, pos in enumerate(self.__pos_list, 1):
    #         print(num, pos.as_text)
    #     print('-' * 40)
    #     print("Result: ", self.amount_result)
    #     print('*' * 40)

    @property
    def as_text(self):
        common_header = self.common_hdr_as_text()
        spec_header = self.special_hdr_as_text()
        positions = self.pos_as_text()
        result = common_header
        if spec_header is not None:
            result += '\n' + spec_header
        result += '\n' + positions
        return result

    def output(self, file=sys.stdout):
        print(self.as_text, file=file)

    def pos_as_text(self):
        result = []
        for k, pos in enumerate(self.__pos_list, 1):
            result.append(f'{k:2d}. {pos.as_text}')
        result.append('-' * 40)
        result.append(f'Total amount: {self.amount_result}')
        result.append('*' * 40)
        return '\n'.join(result)

    def common_hdr_as_text(self):
        result = []
        result.append('*' * 40)
        result.append(f'Account №: {self.number}')
        with suppress(AttributeError):
            result.append(f'Address: {self.address}')
        result.append(f'Creation time: {self.creation_date}')
        with suppress(AttributeError):
            result.append(f'Is out at: {self.out_time}')
        with suppress(AttributeError):
            result.append(f'Get at: {self.recv_time}')
        with suppress(InsufficientData):
            result.append(f'Way-time: {self.in_way}')
        return '\n'.join(result)

    def special_hdr_as_text(self):
        return None

    def as_xml_part(self, document):
        result = document.createElement(self.xml_tag_name)
        result.setAttribute('number', self.__number)
        common = self.common_xml(document)
        result.appendChild(common)
        special = self.special_xml(document)
        result.appendChild(special)
        positions = self.positions_xml(document)
        result.appendChild(positions)
        return result

    @property
    def xml_tag_name(self):
        return type(self).__name__

    def common_xml(self, document):
        result = document.createElement('common')
        try:
            data = self.__address
            addr_text = document.createTextNode(data)
            address = document.createElement('address')
            address.appendChild(addr_text)
            result.appendChild(address)
        except AttributeError:
            pass
        result.setAttribute('created', self.__creation_date.isoformat())
        with suppress(AttributeError):
            result.setAttribute('out_time', self.__out_time.isoformat())
        with suppress(AttributeError):
            result.setAttribute('received', self.__recv_time.isoformat())
        return result

    def special_xml(self, document):
        result = document.createElement('special')
        return result

    def positions_xml(self, document):
        result = document.createElement('positions')
        for pos in self.__pos_list:
            pos_xml = pos.as_xml_part(document)
            result.appendChild(pos_xml)
        return result

    def common_from_xml(self, common):
        self.__creation_date = datetime.fromisoformat(common.getAttribute('created'))
        if common.hasAttribute('recv_time'):
            recv_time = common.getAttribute('recv_time')
            self.__recv_time = datetime.fromisoformat(recv_time)
        if common.hasAttribute('out_time'):
            out_time = common.getAttribute('out_time')
            self.__out_time = datetime.fromisoformat(out_time)

    @classmethod
    def from_xml(cls, element):
        if element.tagName != cls.__name__:
            raise ValueError(f'Infalid tag "{element.tagName}"')
        result = AccountPayment()
        result.__number = element.getAttribute('number')
        # common = element.getElementsByTagName('common').item(0)
        # result.common_from_xml(common)