# !/usr/bin/env python
# -*- coding: utf:8 -*-

from .AccountPayment import AccountPayment
from .AccountLimited import AccountPaymentLimited


def create_from_xml(element):
    if element.tagName == 'AccPos':
        return AccountPayment.from_xml(element)
    if element.tagName == 'AccountPaymentLimited':
        return AccountPaymentLimited.from_xml(element)
