# /usr/bin/env python3
# -*- coding: utf-8 -*-
from .AccountPayment import AccountPayment
from datetime import timedelta, datetime
from Helpers.exceptions import InsufficientData


class AccountPaymentLimited(AccountPayment):
    def __init__(self, hours: int):
        super().__init__()
        self.__limit = timedelta(hours=hours)

    @property
    def limit(self):
        return self.__limit

    @property
    def deadline(self):
        try:
            return self.out_time + self.limit
        except AttributeError as Exp:
            raise InsufficientData('out_time') from Exp

    @property
    def outdate(self):
        if not self.is_out:
            return False
        if self.is_received:
            return self.recv_time >= self.deadline
        return datetime.now() >= self.deadline

    def special_hdr_as_text(self):
        result = []
        try:
            result.append(f'Deliver to: {self.deadline}')
        except InsufficientData:
            result.append(f'Estimated delivery time: {self.limit}')
        if self.outdate:
            result.append('***Expired***')
        return '\n'.join(result)

    @property
    def is_good(self):
        if not super().is_good():
            return False
        else:
            return not self.outdate

    def special_xml(self, document):
        result = document.createElement('special')
        result.setAttribute('hours', str(round(self.__limit.total_seconds() / 3600)))
        return result
