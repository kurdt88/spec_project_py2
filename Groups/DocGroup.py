# !/usr/bin/env python
# -*- coding: utf:8 -*-

from collections.abc import Sequence

from Account import CreateFromXML
from Helpers.exceptions import ErrorInProgram
from xml.dom.minidom import parseString
from xml.dom.minidom import Node


class DocGroup(Sequence):

    def __init__(self):
        self.__doc_list = []

    def __len__(self):
        return len(self.__doc_list)

    def __get_item_by_index(self, index):
        if len(self.__doc_list) <= 0:
            raise IndexError(index)
        if index >= len(self.__doc_list):
            raise IndexError(index)
        if index < -len(self.__doc_list):
            raise IndexError(index)
        if index < 0:
            index += len(self.__doc_list)
        min_date = min((doc.creation_date for doc in self.__doc_list))
        for _ in range(0, len(self.__doc_list)):
            if index == 0:
                for doc in self.__doc_list:
                    if doc.creation_date == min_date:
                        return doc
                else:
                    raise ErrorInProgram('A-1')
            try:
                min_date = min((doc.creation_date
                                for doc in self.__doc_list
                                if doc.creation_date > min_date))
            except ValueError as Exc:
                raise ErrorInProgram('A-3') from Exc
            index -= 1
        else:
            raise ErrorInProgram('A-2')

    def __get_item_by_number(self, number):
        for doc in self.__doc_list:
            if number == doc.number:
                return doc
        else:
            raise KeyError(number)

    def __getitem__(self, index):
        if isinstance(index, int):
            return self.__get_item_by_index(index)
        else:
            return self.__get_item_by_number(number=index)

    def add(self, doc):
        #        if doc not in self:
        self.__doc_list.append(doc)

    @property
    def as_xml(self):
        result = parseString(
            '<?xml version="1.0" encoding="utf-8" standalone="yes"?><docgroup/>'
        )
        root = result.documentElement
        for doc in self:
            elem = doc.as_xml_part(result)
            root.appendChild(elem)
        return result

    @property
    def as_xml_text(self) -> str:
        return self.as_xml.toprettyxml(indent='    ')

    @classmethod
    def from_xml(cls, element):
        if element.tagName != 'docgroup':
            raise ValueError(f'Invalid tag name "{element.tagName}"')
        result = DocGroup()
        for elem in element.childNodes:
            if elem.nodeType != Node.ELEMENT_NODE:
                continue
            doc = CreateFromXML.create_from_xml(elem)
            result.add(doc)
        return result
