# /usr/bin/env python3
# -*- coding: utf-8 -*-
from .AccountPosition import AccPos, COPEECK
from decimal import Decimal


class AccPosAmount(AccPos):
    def __init__(self, title, count, amount):
        super().__init__(title, count, Decimal('0.0000'))
        self.__amount = Decimal(amount).quantize(COPEECK)

    @property
    def as_text(self):
        return f'{self.title} {self.count} "--" {self.amount}'

    @property
    def price(self):
        return self.amount / self.count

    @property
    def amount(self):
        return self.__amount

    def as_xml_part(self, document):
        # result = document.createElement(self.xml_tag_name)
        # # result.setAttribute('title', self.title)
        # title = document.createTextNode(self.title)
        # result.appendChild(title)
        # result.setAttribute('count', str(self.count))
        # result.setAttribute('price', str(round(self.price, 2)))
        # result.setAttribute('amount', str(self.__amount))
        result = super().as_xml_part(document)
        result.setAttribute('amount', str(self.__amount))
        result.removeAttribute('price')
        return result
