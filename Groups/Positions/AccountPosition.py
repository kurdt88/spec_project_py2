# /usr/bin/env python3
# -*- coding: utf-8 -*-
from decimal import Decimal

COPEECK = Decimal('0.01')
PRICE = Decimal('0.0001')


class AccPos(object):
    def __init__(self, title: str,
                 count: Decimal,
                 price: Decimal):
        self.__title = AccPos.__init__.__annotations__['title'](title).strip()
        self.__count = AccPos.__init__.__annotations__['count'](count)
        self.__price = AccPos.__init__.__annotations__['price'](price).quantize(Decimal(PRICE))

    @property
    def title(self):
        return self.__title

    @property
    def count(self):
        return self.__count

    @count.setter
    def count(self, new_count):
        self.__count = Decimal(new_count)

    @property
    def price(self):
        return self.__price

    @price.setter
    def price(self, new_price):
        self.__price = Decimal(new_price).quantize(PRICE)

    @property
    def amount(self):
        result = (self.count * self.price).quantize(COPEECK)
        if result < COPEECK:
            return COPEECK
        return result

    @property
    def as_text(self):
        # форматирование c 3.6 :
        return f'{self.title} {self.count} {self.price} {self.amount}'
        # форматирование до 3.6 :
        # return '{0.title} {0.count} {0.price} {0.amount}'.format(self)

    @property
    def is_good(self):
        return len(self.title) > 0

    @property
    def xml_tag_name(self):
        return type(self).__name__

    def as_xml_part(self, document):
        result = document.createElement(self.xml_tag_name)
        title = document.createTextNode(self.__title)
        result.appendChild(title)
        result.setAttribute('price', str(self.__price))
        result.setAttribute('count', str(self.__count))
        result.setAttribute('amount', str(self.amount))
        return result

    # data = self.__address
    # addr_text = document.createTextNode(data)
    # address = document.createElement('address')
    # address.appendChild(addr_text)
    # result.appendChild(address)
