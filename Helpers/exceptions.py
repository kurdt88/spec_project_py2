# !/usr/bin/env python3
# -*- coding -*-


class ErrorInProgram(Exception):
    pass


class InsufficientData(Exception):
    pass
